package mage.Tools;

public final class Constants {

	// Defaults
	public static String makeblastdb = "/usr/local/ncbi/blast/bin/makeblastdb" ; 
	public static String blastn = "/usr/local/ncbi/blast/bin/blastn";
	public static String MFOLD = "/usr/local/bin/hybrid-ss-min";
	
	public static String workingdirectory = System.getProperty("user.dir")+"/";
	
	public static String targets = "";
	public static String parameters = "";
	public static String switches = "";
	
}
