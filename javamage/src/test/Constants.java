package test;

public class Constants {

	final public static String ioDirectory ="/Users/mockingbird/dropbox/research/optimization/io_testing/";
	final public static String naturalTestDirectory = "/Users/mockingbird/dropbox/research/optimization/natural/";
	final public static String bodirectory = "/Users/mockingbird/dropbox/research/optimization/mt_testing/";
	final public static String bo_testing= "/Users/mockingbird/dropbox/research/optimization/bo_testing/";
	final public static String blastdirectory = "/Users/mockingbird/dropbox/research/optimization/blast/";
	final public static String optMagedirectory = "/Users/mockingbird/dropbox/research/optimization/optmage_testing/";
}
